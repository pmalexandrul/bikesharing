import numpy as np


def select_closest(candidates, origin, vanCapacity):
    """Return the index of the closest candidate to a given point."""
    print(candidates)
    distance = euclidean_distance(candidates, origin).argmin()

    return distance

def euclidean_distance(a, b):
    """Return the array of distances of two numpy arrays of points."""

    distance = np.linalg.norm(a - b, axis=1)
    return distance

def route_distance(cities):
    """Return the cost of traversing a route of cities in a certain order."""
    points = cities[['latitude', 'longitude']]
    distances = euclidean_distance(points, np.roll(points, 1, axis=0))
    return np.sum(distances)
