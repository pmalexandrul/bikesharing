import pandas as pd
import numpy as np

def read_stations(filename):
    """
    Read a file in .tsp format into a pandas DataFrame

    The .tsp files can be found in the TSPLIB project. Currently, the library
    only considers the possibility of a 2D map.
    """
    with open(filename) as f:
        node_coord_start = None
        dimension = None
        lines = f.readlines()

        # Obtain the information about the .tsp
        i = 0
        while not dimension or not node_coord_start:
            line = lines[i]
            if line.startswith('DIMENSION :'):
                dimension = int(line.split()[-1])
            if line.startswith('NODE_COORD_SECTION'):
                node_coord_start = i
            i = i+1

        print('Problem with {} stations read.'.format(dimension))

        f.seek(0)

        # Read a data frame out of the file descriptor
        cities = pd.read_csv(
            f,
            skiprows=node_coord_start + 1,
            sep=' ',
            names=['station', 'latitude', 'longitude'],
            dtype={'station': str, 'latitude': np.float64, 'longitude': np.float64},
            header=None,
            nrows=dimension
        )

        # cities.set_index('city', inplace=True)

        return cities

def createStationsWithDemand(stationsFile, demandFile):

    demand = pd.read_csv(demandFile)
    stations = read_stations(stationsFile)
    demandList = []


    for index, row in stations.iterrows():
        i = 0
        for index1, row1 in demand.iterrows():
            if (row['station'] == row1['day']):
                demandList.append(row1['Friday'])
                i = 1
        if (i == 0):
            demandList.append(0)
    stations['demand'] = demandList
    indexArray = []
    for index, row in stations.iterrows():
        if(int(float(row['demand'])) > 20):
            i = 0
            while int(float(row['demand'])) > 20:
                row['demand'] = int(float(row['demand'])) - 20
                x = row['demand'] % 20
                if( i == 0):
                    rowWithTheRest1 = {'station': row['station'], 'latitude': row['latitude'],
                                   'longitude': row['longitude'],
                                   'demand': x}
                    stations = stations.append(rowWithTheRest1, ignore_index=True)
                    i = 1
                new_row = {'station':row['station'], 'latitude':row['latitude'], 'longitude':row['longitude'], 'demand':'20'}
                stations = stations.append(new_row,ignore_index=True)
                indexArray.append(index -1)

        if (int(float(row['demand'])) <= -20):
            i = 0
            while int(float(row['demand'])) <= -20:
                 row['demand'] = int(float(row['demand']))+20
                 x = -20 + (row['demand'] % 20)
                 if (i == 0):
                     rowWithTheRest1 = {'station': row['station'], 'latitude': row['latitude'],
                                        'longitude': row['longitude'],
                                        'demand': x}
                     stations = stations.append(rowWithTheRest1, ignore_index=True)
                     i = 1
                 new_row = {'station':row['station'], 'latitude':row['latitude'], 'longitude':row['longitude'], 'demand':'-20'}
                 stations = stations.append(new_row,ignore_index=True)
                 indexArray.append(index - 1)

    stations = stations.drop(indexArray)
    print(stations)
    return stations

def normalize(points):
    """
    Return the normalized version of a given vector of points.

    For a given array of n-dimensions, normalize each dimension by removing the
    initial offset and normalizing the points in a proportional interval: [0,1]
    on y, maintining the original ratio on x.
    """
    ratio = (points.latitude.max() - points.latitude.min()) / (points.longitude.max() - points.longitude.min()), 1
    ratio = np.array(ratio) / max(ratio)
    norm = points.apply(lambda c: (c - c.min()) / (c.max() - c.min()))
    return norm.apply(lambda p: ratio * p, axis=1)
