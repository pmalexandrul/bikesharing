import numpy as np

from io_helper import createStationsWithDemand, normalize
from neuron import generate_network, get_neighborhood, get_route
from distance import select_closest, euclidean_distance, route_distance
from plot import plot_route

def main():
    stations = createStationsWithDemand("../assets/nyStations.tsp", "../assets/demand_3_hours_IDS.csv")
    print(stations)

    route = som(stations, 100000)

    problem = stations.reindex(route)

    distance = route_distance(problem)

    print('Route found of length {}'.format(distance))


def som(stations, iterations, learning_rate=0.8):
    """Solve the TSP using a Self-Organizing Map."""

    # Obtain the normalized set of cities (w/ coord in [0,1])
    stations = stations.copy()
    stations[['latitude', 'longitude']] = normalize(stations[['latitude', 'longitude']])
    vanCapacity = 20
    # The population size is 8 times the number of cities
    n = stations.shape[0] * 8

    # Generate an adequate network of neurons:
    network = generate_network(n)
    print('Network of {} neurons created. Starting the iterations:'.format(n))

    for i in range(iterations):
        if not i % 100:
            print('\t> Iteration {}/{}'.format(i, iterations), end="\r")
        # Choose a random city
        station = stations.sample(1)[['latitude', 'longitude']].values
        winner_idx = select_closest(network, station, vanCapacity)
        # Generate a filter that applies changes to the winner's gaussian
        gaussian = get_neighborhood(winner_idx, n//10, network.shape[0])
        # Update the network's weights (closer to the city)
        network += gaussian[:,np.newaxis] * learning_rate * (station - network)
        # Decay the variables
        learning_rate = learning_rate * 0.99997
        n = n * 0.9997

        # Check if any parameter has completely decayed.
        if n < 1:
            print('Radius has completely decayed, finishing execution',
            'at {} iterations'.format(i))
            break
        if learning_rate < 0.001:
            print('Learning rate has completely decayed, finishing execution',
            'at {} iterations'.format(i))
            break
    else:
        print('Completed {} iterations.'.format(iterations))

    route = get_route(stations, network)
    print(route)
    plot_route(stations, route, '../diagrams/route.png')
    return route

if __name__ == '__main__':
    main()
