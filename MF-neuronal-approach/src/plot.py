import matplotlib.pyplot as plt
import matplotlib as mpl


def plot_route(stationsDF, route, name='diagram.png', ax=None):
    """Plot a graphical representation of the route obtained"""
    mpl.rcParams['agg.path.chunksize'] = 10000

    if not ax:
        fig = plt.figure(figsize=(5, 5), frameon=False)
        axis = fig.add_axes([0, 0, 1, 1])

        axis.set_aspect('equal', adjustable='datalim')
        plt.axis('off')

        axis.scatter(stationsDF['latitude'], stationsDF['longitude'], color='red', s=4)
        for index, row in stationsDF.iterrows():
            axis.annotate(row['station'], (row['latitude'], row['longitude']))
        route = stationsDF.reindex(route)
        print(route['station'])
        i = 0
        for value in route['station']:
            if value[1] == "324":
                route.loc[route.shape[0]] = route.iloc[i]
            i += 1
        axis.plot(route['latitude'], route['longitude'], color='blue', linewidth=1)

        plt.savefig(name, bbox_inches='tight', pad_inches=0, dpi=200)
        plt.close()

    else:
        ax.scatter(stations['latitude'], stations['longitude'], color='red', s=4)
        route = stations.reindex(route)
        route.loc[route.shape[0]] = route.iloc[0]
        ax.plot(route['latitude'], route['longitude'], color='purple', linewidth=1)
        return ax
