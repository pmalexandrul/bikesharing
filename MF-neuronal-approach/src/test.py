import pandas as pd
import numpy as np
from io_helper import read_stations
demand = pd.read_csv("assets/demand_3_hours_IDS.csv")
stations = read_stations("assets/nyStations.tsp")

demandList = []

for index,row in stations.iterrows():
    i=0
    for index1,row1 in demand.iterrows():
        if(row['station'] == row1['day']):
           demandList.append(row1['Friday'])
           i=1
    if(i == 0):
        demandList.append(0)

stations['demand'] = demandList