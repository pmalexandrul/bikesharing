import numpy as np
import pandas as pd
import datetime as dt

df = pd.read_csv('../datasets/2013_08_NYC.csv')


def prepare_df(df):
    df = df.sort_values(['bikeid', 'starttime']).reset_index()
    df['starttime'] = pd.to_datetime(df['starttime'])
    df['month'] = df['starttime'].dt.month
    df['weeknumber'] = df['starttime'].dt.week
    df['day'] = df['starttime'].dt.day_name()
    df['hour'] = df['starttime'].dt.hour
    df['timeslot'] = np.where(df['hour'] < 12, "00-11", "12-23")

    conditions = [
        df['hour'].between(0, 2),
        df['hour'].between(3, 5),
        df['hour'].between(6, 8),
        df['hour'].between(9, 11),
        df['hour'].between(12, 14),
        df['hour'].between(15, 17),
        df['hour'].between(18, 20),
        df['hour'].between(21, 23)
    ]

    values = ["00-02", "03-05","06-08","09-11","12-14","15-17","18-20","21-23"]

    #df['timeslot'] = np.select(conditions, values)

    return df


def generate_crosstab(df):
    demand = pd.crosstab(df['start station name'], [df['month'], df['day'], df['timeslot']])/4
    supply = pd.crosstab(df['end station name'], [df['month'], df['day'], df['timeslot']])/4
    cum_demand = np.rint(supply - demand)
    return cum_demand


# df['redist'] = np.select(conditions,values)
df = prepare_df(df)
crosstab = generate_crosstab(df)


#Get demand for specific time: demand.loc['Cherry St', (8, 31, 'Sunday', 'After 12')]

