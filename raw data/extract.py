import pandas as pd

data_raw = pd.read_csv('2013-08 - Citi Bike trip data.csv', header=0)
df_raw = pd.DataFrame(data_raw)
df_raw = df_raw[['end station name','end station id']]
df_raw = df_raw.drop_duplicates()

demand_data = pd.read_csv('../demand_3_hour.csv', header=[0,1,2,3], index=False)
df_demand = pd.DataFrame(demand_data)
for index,row in df_demand.iterrows():
    otherrow = df_raw.loc[row['month'] == df_raw['end station name']]
    if otherrow.size!=0:
        a = otherrow['end station id'].item()
        row['month'] = a
print (df_demand.head())
df_demand.to_csv("demand_with_station_id.csv")