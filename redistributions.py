import numpy as np
import pandas as pd
import datetime as dt

df = pd.read_csv('raw_data/2013_08_NYC.csv')
df = df.sort_values(['bikeid', 'starttime']).reset_index()
df['starttime'] = pd.to_datetime(df['starttime'])
df['month'] = df['starttime'].dt.month
df['weeknumber'] = df['starttime'].dt.week
df['day'] = df['starttime'].dt.day_name()
df['hour'] = df['starttime'].dt.hour
df['timeslot'] = np.where(df['hour'] < 12, "Before 12","After 12")


#df = df[['start station id', 'start station latitude', 'start station longitude', 'month', 'day', 'hour', 'end station id']]
stations = df[['start station id','start station name', 'start station latitude','start station longitude']].drop_duplicates().set_index('start station id')



starts = df['start station id'].value_counts()
ends = df['end station id'].value_counts()

df['shift'] = df['end station id'].shift(1, axis=0)
df['diff'] = df['start station id'] - df['shift']
df['cumcount'] = df.groupby('bikeid').cumcount()

conditions = [
    ((abs(df['diff']>0)) & (df['cumcount']>0)),
    ((abs(df['diff'] <= 0)) & (df['cumcount'] <= 0))
              ]

values = [1,0]
df['redist'] = np.select(conditions,values)
redist_sum = df['redist'].sum()

combined = pd.DataFrame()
combined['starts'] = starts
combined['ends'] = ends
count_redist = df[['start station id','redist']].groupby('start station id').sum()

stats = combined.join(stations, lsuffix='_caller', rsuffix='_other')
stats = stats.join(count_redist)

stats['delta'] = stats['starts'] - stats['ends']
stats['relative'] = abs(stats['delta'] / (stats['starts'] + (stats['ends'])))
stats.sort_values('redist', inplace=True)
